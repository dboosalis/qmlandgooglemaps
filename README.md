# README #
This README is part of the qmlWithGoogleMaps demo, which can be found at: https://bitbucket.org/dboosalis/qmlandgooglemaps
To use this code you will need to provide a Google Maps API Key. This key should be placed into the html/googleMaps.html file.
This code has been tested with Qt version 5.10 on Linux and Mac, as well as with Boot2Qt on a Raspberry Pi 3.

### What is this repository for? ###
The purpose of this repository is to provide a simple example on how to incorporate Google Maps, into a QML application.  
* Quick summary
By providing a valid Google Map API Key, And loading the project file into Qt's Creator IDE (or using command line qmake & make) you should be able to see a Google Map.  In this map there will be a Toolbar containing one menu, and in this one menu is one item labeled "Coffee Search".  These items are all done in QML, but when you click on the menu item.  It will find all the coffee shops from Google Place Services and populate these Google Results in a QML ListView.  By Selecting the directions icon on any item in the list, will tell the Google Map to show directions to that coffee place from the given location. 


Description.
This application uses Qt's WebEngine to display a Google Map, and also uses Qt's Javascript class "qwebchannel.js" to facilitate communication between the QML layer and the webengine.  This app demonstrates a three layer communication between Qt's C++, QML, and the WebEngine.  As an example: the zoom layer is read from QSettings on startup and feed to the QML which feeds it to the Webengine.  And when one changes the zoom level of the map, this signal gets sent up to the QML, which sends it up to the Qt layer for saving it as a QSettings for next use.  


* Version 1.0


* References: 
 Qt WebChannel Doc 	http://doc.qt.io/qt-5/qtwebchannel-index.html
 Qt WebEngine		http://doc.qt.io/qt-5/qtwebengine-index.html
 Google Maps API	https://developers.google.com/maps/documentation/javascript/tutorial
 Getting a key		https://developers.google.com/maps/documentation/javascript/get-api-key

* Repo owner or admin
  David Boosalis, david.boosalis@gmail.com

* Other community or team contact
